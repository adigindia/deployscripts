# set the base image
# FROM adigindia/cpdoftomcat:latest

FROM tomcat:latest

# author
MAINTAINER Aditya Garg

# extra metadata
LABEL version="1.1"
LABEL description="Image with Dockerfile. and running tomcat by default"

EXPOSE  8080

COPY ./CounterWebApp.war /usr/local/tomcat/webapps/
